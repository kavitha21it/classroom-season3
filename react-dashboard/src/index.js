import React from 'react';
import ReactDOM from 'react-dom';
/* import './index.css';
import App from './App'; */
import * as serviceWorker from './serviceWorker';
import Main from './App';

//const rootElement = document.getElementsByClassName('dashboard-app')[0];
//ReactDOM.render(<App/>, rootElement);

// ReactDOM.render(<small>Hello React</small>, document.getElementById('weather-widget'));

//ReactDOM.render(<Main/>,document.getElementById('weather-widget'));

ReactDOM.render(<Main/>,document.getElementById('root'));



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
