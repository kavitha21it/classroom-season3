import React from 'react';
import { BrowserRouter } from "react-router-dom";
import {
    Link
} from 'react-router-dom';

function Input() {
    return (
        <main>
            <div className="d-flex justify-content-center align-items-center w-auto min-vh-100 mt-lg-n4">
                <form className="w-75">
                    <div className="container-fluid flex-column text-center mt-1">
                        <p className="font-weight-bold lead">Entry level Software Engineer<br></br>
                            Charlotte, NC<br></br>
                            Description, etc
                        </p>
                    </div>
                    <div className="form-group row">
                        <label for="input-lastname"
                            className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold m-0">First
                    Name<sup>*</sup></label>
                        <div className="col-sm-4" m-0>
                            <input type="text" className="form-control" id="input-firstname" placeholder="First Name" />
                        </div>
                        <label for="input-lastname"
                            className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold m-0">Last
                    Name<sup>*</sup></label>
                        <div className="col-sm-4 m-0">
                            <input type="text" className="form-control" id="input-lastname" placeholder="Last Name" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label for="input-email"
                            className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold">Email<sup>*</sup></label>
                        <div className="col-sm-10">
                            <input type="email" className="form-control" id="input-email" placeholder="Email" />
                        </div>
                    </div>
                    <div className="form-group row d-flex">
                        <label for="input-pri-phone"
                            className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold">Primary
                    Phone </label>
                        <div className="col-sm-4">
                            <input type="tel" className="form-control" id="input-sec-phone" placeholder="Phone #" />
                        </div>
                        <label for="input-sec-phone"
                            className="col-sm-2 col-form-label text-right mr-auto pt-3 pb-3 is-invalid is-valid font-weight-bold">Secondary
                    Phone</label>
                        <div className="col-sm-4">
                            <input type="tel" className="form-control" id="input-sec-phone" placeholder="Secondary Phone #" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label for="input-linkedin"
                            className="col-sm-2 col-form-label text-right pt-3 pb-3 is-invalid is-valid font-weight-bold">LinkedIn
                    Profile</label>
                        <div className="col-sm-10">
                            <input type="text" className="form-control" id="input-linkedin" placeholder="Linkedin Document" />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label for="resume-upload"
                            className="col-sm-3 col-form-label text-right pt-3 pb-3 font-weight-bold">Resume<sup>*</sup></label>
                        <div className="custom-file file-field col-sm-9">
                            <input type="file" className="" id="resume-upload" />
                        </div>
                    </div>
                    <div className="row float-right">
                        <button className="btn btn-info my-2 my-sm-0" id="submit" type="reset">Submit</button>
                        <a href="page1_vs2.html" className="btn btn-primary my-2 my-sm-0 ml-3 border-info text-light ">Cancel</a>
                    </div>
                </form>
            </div>
        </main>
        )};

export default Input;
        