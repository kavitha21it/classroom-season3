const assignments=[{
    imagesrc: "https://placeimg.com/640/480/any",
    title:"HTML tags by category",
    desc: "Inline elements vs Block elements",
    createdon: "sep 8th 2019",
    alttext: "HTML by tags assignment",
    buttontxt: "UpVote",
    link :"../assingments/html-tags-by-category.html",

}
];
 const carelements =assignments.map(function(assignment){
 return `<div class="card">
                    <span class="icon">
                        <i class="fa fa-heart"></i>
                    </span>
                    <img src="${assignments.imgsrc}" alt="${assignments.alttext}" />
                    <p class="title">${assignments.title}</p>
                    <p class="title">${assignments.desc}</p>
                    <p>Created on : ${assignments.createdon}</p>

                    <footer>
                        <a class="button button-orange">${assignments.buttontxt}</a>
                        <a class="button button-blue" href="${assignments.link}">View</a>
                    </footer>
                    </div>`
 });
 const cardelement=document.getElementsByClassName