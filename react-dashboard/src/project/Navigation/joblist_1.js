import React from 'react';
import '../home/mystyle.css';


function Joblist(props) {
    const initialJobList = [
        {
            title: "Entry level Software Engineer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Junior Java developer",
            location: "Mathews, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Java Developer",
            location: "Mooresville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "SAS Developer",
            location: "Cary, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Salesforce Developer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Junior Salesforce Developer",
            location: "Fayetteville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Marketing Developer",
            location: "Durham, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Mobile Developer",
            location: "Charlotte, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },

        {
            title: "Middle-Tier Developer",
            location: "Greensboro, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Graphics Developer",
            location: "Ashcville, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",

        },
        {
            title: "Big Data Developer",
            location: "Matthews, NC",
            desc: "Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",


        }
    ];


const [jobsList, setJobList] = React.useState(initialJobList);

const initialNewJobList = {
    title: "",
    location: " ",
    desc: " "

};
const [jobListFormVal, setJobListFormVal] = React.useState(props.record);
console.log(props);
return (
    <div>
        <h1>Job Listing</h1>
        <div className="container-fluid">
            <div className="row">
            <div className="col-6">
                        <jobListForm record={initialNewJobList} recordChange={handleNewJobList}/>
                    </div>
                <div className="col-sm-6">
                    <div className="card borderpg2 ml-1 mr-1 mt-3">
                        <div className="card-body d-flex flex-column" style="max-height: 214px;">
                            <a className="card-title h5 title" href="#" href="#" data-toggle="modal"
                                data-target="#entry-level-software-application">{jobListFormVal.title}</a>
                            <a className="card-sub title mb-2 h6 ml-1 row text-muted mt-2">{jobListFormVal.location}</a>
                            <p className="card-text">{jobListFormVal.desc}</p>
                            <div className="d-flex mt-auto">
                                <a href="Input_pg.html" className="btn apply text-light ">Apply</a>
                                <a href="#" className="btn view border-info ml-auto text-white" data-toggle="modal"
                                    data-target="#entry-level-software-application">View Details</a>
                            </div>
                        </div>
                        // Modal START 
                        <div className="modal fade" id="entry-level-software-application" tabindex="-1" role="dialog"
                            aria-labelledby="entry-level-software-application-title" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title" id="entry-level-software-application-title">
                                            {jobListFormVal.title}</h5>
                                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div className="modal-body">
                                        <h1>{jobListFormVal.desc}</h1>
                                        <img src="#"
                                            style="width:250px;" />
                                    </div>
                                    <div className="modal-footer d-flex mt-auto">
                                        <a href="Input_pg.html" className="btn apply text-light ">Apply</a>
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

function handleFormSubmit(jobListFormVal) {
console.log ('Handle new jobList', jobListFormVal); 
    //   $event.preventDefault();
   const oldJobList = jobsList;
   const newJobList = [jobListFormVal]
   const newJobLists = oldJobList.concat(newJobList)
   setJobList(newJobList);
   setJobListFormVal(initialJobList);
}

function handleNewJobList(){

}
/*function handleTitleChange($event) {
//  setJobListFormVal({
//  title: $event.target.value,
//  location: jobListFormVal.location,
//  desc: jobListFormVal.desc,
//    })
//}

//function handleTitleChange($event) {
//    setJobListFormVal({
//    title: jobListFormVal.title,
//    location: $event.target.value,
//    desc: jobListFormVal.desc,
//    })
//}

//function handleTitleChange($event) {
//    setJobListFormVal({
//    title: jobListFormVal.title,
//    location: jobListFormVal.location,
//    desc: $event.target.value, 
   })
}*/
}

export default Joblist;

