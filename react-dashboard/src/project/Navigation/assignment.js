import React from 'react';
import AssignmentForm from './AssignmentForm';

function Assignments() {
    /* const initialAssignments = [
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Tags by category",
            desc: "Inline elements vs Block Elements",
            createdOn: "Sep 8th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Get Started",
            desc: "Button in html / css",
            createdOn: "Sep 10th 2019"
        },
        {
            imageSrc: "https://placeimg.com/140/80/any",
            title: "Html Inline elements",
            desc: "Html Inline elements",
            createdOn: "Sep 14th 2019"
        }
    ]; */


    const [assignments, setAssignments] = React.useState([]);

    const initialNewAssignment = {
        imageSrc: "https://placeimg.com/140/80/any",
        title: "",
        desc: "",
        createdOn: ""
    };

    React.useEffect(() => {

        // console.log(window.firebase.database());
        const assignmentRef = window.firebase.database().ref('/assingments');

        assignmentRef.on('value', function (snapshot) {
            const assingmentsFromDB = snapshot.val();
            setAssignments(assingmentsFromDB);
            console.log('assignments from db', assingmentsFromDB);
        });

    }, []);

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-3">
                        <AssignmentForm record={initialNewAssignment} recordChange={handleNewAssignment} />
                    </div>
                    <div className="col-9">
                        <div className="row">
                            {

                                assignments.map((assignment, index) => {
                                    return <div className="col-sm-12 col-md-6 col-lg-4" key={index}>
                                        <div className="card">
                                            <img src={assignment.imageSrc} className="card-img-top" alt="" />
                                            <div className="card-body">
                                                <h5 className="card-title">{assignment.title}</h5>
                                                <p className="card-text">Created on : {assignment.createdOn}</p>
                                                <div className="d-flex">
                                                    <a href="#" className="btn mr-auto btn-primary">UpVote</a>
                                                    <a href="#" className="btn btn-info">View</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                })

                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

    function handleNewAssignment(assignmentFormVal) {
        console.log('Handle new assignment', assignmentFormVal);
        const oldAssignments = assignments;
        /* const newAssignment = [assignmentFormVal]
        const newAssignments = oldAssignments.concat(newAssignment)
        setAssignments(newAssignments); */


        const updatedRecord = {};
        updatedRecord['/assingments/' + oldAssignments.length] = assignmentFormVal;
        window.firebase.database().ref().update(updatedRecord);

    }


}

export default Assignments;