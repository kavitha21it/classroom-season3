import React from 'react';

function Footer(){
    const currentYear = new Date().getFullYear();
    // console.log('current year', currentYear);
    return (
        <footer className="navbar navbar-light light-blue fixed-bottom">
           <p className="m-auto">Copyright&copy; MissionCode {currentYear}</p>
        </footer>
    );
}

export default Footer;