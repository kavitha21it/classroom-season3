/*Navigation bar starts here*/
/*LOGO*/
let logoimage= `<img src="../Image/mission-code-logo.png" alt="missioncode logo" width="150" height="50">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarText">                
</div>`;
let navlogo = document.getElementById("navbarLogo"); 
navlogo.innerHTML=logoimage;

/*NAVIGATION LIST*/

const navitems = [{
        text: "Home",
        link: "#",
        class: "nav-link",
        pclass: "nav-item",
        isActive: true
    },
    {
        text: "Profile",
        link: "#",
        class: "nav-link",
        pclass: "nav-item",
        isActive: false
    },
    {
        text: "Log-out",
        link: "#",
        class: "nav-link",
        pclass: "nav-item",
        isActive: false
    }
];

    const menuElement = document.getElementById("navbarText");
    /* console.log('Before Adding dynamically');
    console.log(menuElement); */
    let menuItemElement = "<ul class=\"navbar-nav ml-auto\">";
    navitems.forEach(function (navItem) {

        // <a class="menu-item" href="">Home</a>
        menuItemElement = menuItemElement + '<li class=' + navItem.pclass + '">' + '<a class="' + navItem.class + '" href="' + navItem.link + '">' + navItem.text + '</a>' + '</li>';

    });

    menuItemElement = menuItemElement + "</ul>"
    // console.log("Nav Items", menuItemElement);
    menuElement.innerHTML = menuItemElement;
    /*Navigation bar ends here*/

  
