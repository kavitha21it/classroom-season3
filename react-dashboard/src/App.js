/*import React from 'react';
import logo from './logo.svg';
import './App.css';
import 

/*function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}*
export default App;*/

//classwork

/*import React from 'react';
import Home from './pages/Home/Home';

function Main() {
  return (
    <Home/>
  );
}

export default Main;*/

//project work

import React from 'react';
import Home from './project/Home/Home';
import Nav from './project/Navigation/Nav'
import Footer from './project/Navigation/Footer';
import Joblist from './project/Navigation/Joblist';
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom';


function Main(){
  return(
    <BrowserRouter>
    <main>
    <Nav />
          <Switch>          
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/joblist">
          <Joblist />
        </Route>
        <Route path="/">
          <Home />
        </Route>    
      </Switch>
      <Footer />
    </main>
  </BrowserRouter>    
  );
}

export default Main;


