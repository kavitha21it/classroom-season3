import React from 'react';

function Search(){
    return(
<div className="d-flex flex-column align-items-center justify-content-center align-middle ">
        <div className="mt-5 pt-5">
            <img src="http://www.missioncode.co/img/logo.png" alt="missioncode logo" width="400" height="150"></img>
        </div>
        <div>
            <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2 " type="search"
                    placeholder="Search for job postings,job title or company" aria-label="Search" size="50"></input>
                <a href="#" className="btn btn-outline-info my-2 my-sm-0">Search</a>
            </form>
        </div>
        </div>
    );
}
export default Search;