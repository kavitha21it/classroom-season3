import React from 'react';
import {
    Link
} from 'react-router-dom';

function Nav(){
return(
    <nav className="navbar navbar-expand-lg navbar-dark bg-info mb-5">
        <a className="navbar-brand" href="#"> Welcome to Missioncode</a>
        
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
    </button>
        <div className="collapse navbar-collapse" id="navbarText">
            <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                    <Link className="nav-link" to="/Home">Home <span className="sr-only">(current)</span></Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/Joblist">Profile</Link>
                </li>
                <li className="nav-item active">
                    <Link className="nav-link" to="/Home">Logout</Link>
                </li>
            </ul>
        </div>
    </nav>
)
}
export default Nav;
    