/**
 * Assignment 1 DayGreeting
 */
var currentdate = new Date();
DayGreeting(currentdate);

function DayGreeting(currentdate) {
    var hour = currentdate.getHours();
    var minute = currentdate.getMinutes();
    if (hour < 12) {
        console.log("Good Morning");
    } else if (hour === 12 && minute === 00) {
        console.log("Good Noon");
    } else if (hour >= 12 && hour <= 15) {
        console.log("Good Afternoon");
    } else if (hour >= 16 && hour <= 18) {
        console.log("Good Evening");
    } else {
        console.log("Good Night");
    }

}
/**********************************************************/
/**
 * Assignment 2 Day Name
 */
var weekday = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
    "Friday", "Saturday")
console.log("Today is " + weekday[currentdate.getDay()])

var somedate = new Date(2019, 12, 28, 0, 0, 0, 0);
console.log("Day of the Random date provided is " + weekday[somedate.getDay()])
/**********************************************************/
/**
 * Assignment 3 find no of Weekend
 */
var schedule = ['Jan 11 2018',
    'Feb 11 2018',
    'Mar 11 2018',
    'Apr 11 2018',
    'May 11 2018',
    'Jun 11 2018',
    'Jul 11 2018',
    'Aug 11 2018',
    'Sep 11 2018',
    'Oct 11 2018',
    'Nov 11, 2018',
    'Dec 11, 2018'];

var count = 0;
for (var i = 0; i < schedule.length; i++) {
    var date = new Date(schedule[i]);
    const daynumber = date.getDay();
    if (daynumber === 0 || daynumber === 6) {
        count++;
    }
}
console.log("No of weekends in the given array is ",count);
/**********************************************************/
/**
 * Assignment 4 print range of numbers
 */
function printRange(rangeStart, rangeStop) {
    var rangevalue = "";
    for (var i = rangeStart; i <= rangeStop; i++) {
        rangevalue += i + ',';
    }

    return rangevalue.slice(0,-1);
}
console.log("numbers between 7 and 121 are",printRange(7, 121));
/**********************************************************/
/**
 * Assignment 5 Find avg
 */
const orderList = [13.98, 34.90, 44, 89.98, 234.23, 111.88];
var total = 0;
for (var i = 0; i < orderList.length; i++) {
    total += orderList[i];
}
var avg = total / orderList.length;
console.log("Avg of the given array is",avg);
/**********************************************************/
/**
 * Assignment 6 Find total price
 */
const orderList1 = [13.95,34.90,89.98,234.23,111.88];
var subtotal=0;
for (var i = 0; i < orderList1.length; i++) {
    subtotal += orderList1[i];
}
const calctax= subtotal*5/100;
const total1= subtotal + calctax;
console.log("Subtotal =", subtotal,"Tax =", calctax,"Total =",total1);