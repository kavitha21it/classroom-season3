import React from 'react';
import {
    Link
} from 'react-router-dom';

function Joblistfn(){

    const InitialJobList = [
        {
            title: "Entry level Software Engineer",
            location: "Charlotte, NC",
            desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
            salary: "$50000 -$60000",
            dateposted : "11/13/19",
            recruiter: "John"
    
        },
        {
            title: "Junior Java developer",
            location: "Mathews, NC",
            desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities.",
            salary: "$50000 -$60000",
            dateposted : "11/13/19",
            recruiter: "John"
        },
        {
            title: "Junior Salesforce Developer",
            location: "Fayetteville, NC",
            desc:"Basic coding experience or Basic knowledge of software programming or knowledge of any programming and Good analytical and problem-solving abilities." ,
            salary: "$50000 -$60000",
            dateposted : "11/13/19",
            recruiter: "John"
        },
        {
            title: "Full-Stack Developer",
            location: "Matthews, NC",
            desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
            salary: "$50000 -$60000",
            dateposted : "11/14/19",
            recruiter: "Abraham"
        },
        {
          title: "SAS Developer",
          location: "Cary, NC",
          desc:"Must be an expert at all things SAS, including frameworks and object oriented programming (OOP).", 
          salary: "$60000 -$70000",
          dateposted : "11/13/19",
          recruiter: "Tracy"
      },
      {
        title: "Salesforce Developer",
        location: "Charlotte, NC",
        desc:"Must be an expert at all things salesforce, including frameworks and object oriented programming (OOP).", 
        salary: "$50000 -$60000",
        dateposted : "11/15/19",
        recruiter: "Ellen"
    },
    {
      title: "Junior Salesforce Developer",
      location: "Fayetteville, NC",
      desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
      salary: "$50000 -$55000",
      dateposted : "11/17/19",
      recruiter: "Tracy"
    },
    {
    title: "Marketing Manager",
    location: "Durham, NC",
    desc:"Must be an expert at all things JavaScript, including frameworks and object oriented programming (OOP).", 
    salary: "$50000 -$55000",
    dateposted : "11/15/19",
    recruiter: "Ellen"
    }
    ];
    const [JobList, SetJobList] = React.useState(InitialJobList);


    return(
        <div className="row">            
            {
                JobList.map((job,index)=>{
                    return <React.Fragment key={index}>
                    <div className="col-sm-6" >  
                            <div className="card mt-1 border-secondary">
                                    <div className="card-body">
                                        <h5 className="card-title">{job.title}</h5>
                                        <h6 className="card-subtitle mb-1 text-muted">{job.location}</h6>
                                        <p className="card-text">{job.desc}</p>
                                        <Link className="btn btn-info mr-3" to="/Job_form" role="button">Apply</Link>            
                                        <button type="button" className="btn btn-info" data-toggle="modal" data-target={'#example_'+index}>View Details</button>
                                    </div>
                            </div>
                        </div> 
                        <div className="modal fade" id={'example_'+index} role="dialog" aria-labelledby={'example_'+index} aria-hidden="true">
                             <div className="modal-dialog" role="document">
                               <div className="modal-content">
                                 <div className="modal-header">
                                   <h5 className="modal-title" id="exampleModalLabel">{job.title}</h5>
                                   <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                     <span aria-hidden="true">&times;</span>
                                   </button>
                                 </div>
                                 <div className="modal-body">
                                 <table class="table table-borderless">
                                    <tr>
                                        <th>Location:</th>
                                        <td>${job.location}</td>
                                    </tr>
                                    <tr>
                                        <th>Date Posted:</th>
                                        <td>${job.dateposted}</td>
                                    </tr>
                                    <tr>
                                        <th>Recruiter:</th>
                                        <td>${job.recruiter}</td>
                                    </tr>
                                    <tr>
                                        <th>Salary:</th>
                                        <td>${job.salary}</td>
                                    </tr>
                                    <tr>
                                        <th>Description:</th>
                                        <td>${job.desc}</td>
                                    </tr>
                                </table>
                                </div>
                               </div>
                             </div>
                           </div>
                        </React.Fragment>
                })
            }
        </div>
        
    );
}

export default Joblistfn;